Fork of http://code.google.com/p/bibtex-js/

BibTeX-js can parse a BibTeX-file and render it as part of an HTML file.
This way, you can easily add a list of publications to your private homepage or display a list of recommended publications for a seminar.
The way the entries are display can be customized using a simple template system and CSS.